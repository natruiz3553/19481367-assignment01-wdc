//  The table of users
const React = require('react');

// Define the UserTable component
function UserTable(props) {
    if (!props.users) {
        return (
            <div>Click the "Show Users" button to display users</div>
        )
    }

    return (
        <table>
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th>State</th>
            </thead>
            <tbody>
                {props.users.map(user => (
                    <tr>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>{user.state}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

// Export the UserTable component
module.exports = UserTable;
