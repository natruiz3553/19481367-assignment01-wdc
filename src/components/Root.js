// ## Root.js ##
//
// This is our top-level React component which contains all of our other
// components in a tree-like hierarchy. This component is mounted into the
// DOM in "client.js".

const _ = require('lodash');
const React = require('react');

const UserTable = require('./UserTable');

const users = [
  {
    uid: 1,
    email: 'john@dev.com',
    personalInfo: {
      name: 'John',
      address: {
        line1: 'westwish st',
        line2: 'washmasher',
        city: 'wallas',
        state: 'WX'
      }
    }
  },
  {
    uid: 63,
    email: 'a.abken@larobe.edu.au',
    personalInfo: {
      name: 'amin',
      address: {
        line1: 'Heidelberg',
        line2: '',
        city: 'Melbourne',
        state: 'VIC'
      }
    }
  },
  {
    uid: 45,
    email: 'Linda.Paterson@gmail.com',
    personalInfo: {
      name: 'Linda',
      address: {
        line1: 'Cherry st',
        line2: 'Kangaroo Point',
        city: 'Brisbane',
        state: 'QLD'
      }
    }
  }
];

function returnUsers(users) {
  return users.map(user => {
    return {
      name: user.personalInfo.name,
      email: user.email,
      state: user.personalInfo.address.state
    };
  });
}



// Here is where we actually define the Root component. At the moment it just
// contains a single component, TaskList.
class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null
    };
  }

  // Update the set of users for this table.
  showUsers() {
    this.setState({
      users: returnUsers(users)
    });
  }

  sortDisplayedUsers() {
    if (this.state.users)
    {
      this.setState({
        users: _.sortBy(this.state.users, [user => user.name.toLowerCase()])
      });
    }
  }

  hideUsers() {
    this.setState({
      users: null
    });
  }

  render() {
    return (
      <div>
        <input type="button" value="Show Users" onClick={() => this.showUsers()} />
        <input type="button" value="Sort Displayed Users" onClick={() => this.sortDisplayedUsers()} />
        <input type="button" value="Hide Users" onClick={() => this.hideUsers()} />
        <UserTable users={this.state.users} />
      </div>
    );
  }
};

// Export the Root component
module.exports = Root;
